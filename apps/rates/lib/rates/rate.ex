defmodule Rates.Rate do
  @moduledoc """
  The Rate schema
  """
  use Ecto.Schema
  import Ecto.Query, warn: false
  import Ecto.Changeset

  alias __MODULE__

  schema "rates" do
    field(:rate_plan, :string)
    field(:amount, :decimal)
  end

  @doc false
  @spec changeset(%Rate{}, map) :: %Ecto.Changeset{}
  def changeset(rate, attrs) do
    rate
    |> cast(attrs, [
      :rate_plan,
      :amount
    ])
    |> validate_required([:rate_plan, :amount])
  end
end
