defmodule Rates do
  @moduledoc """
  Rates keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  import Ecto.Query, warn: false
  alias Rates.Repo

  alias Rates.Rate

  @doc """
  Returns the list of rates.

  ## Examples

      iex> list_rates()
      [%Rate{}, ...]

  """
  @spec list_rates() :: [%Rate{}]
  def list_rates do
    Rate
    |> Repo.all()
    |> Repo.preload([:roles, :users])
  end

  @doc """
  Create a Rate.
  """

  @spec create_rate(map()) :: {:ok, %Rate{}} | {:error, %Ecto.Changeset{}}
  def create_rate(%{rate_plan: rate_plan, amount: amount} = attrs) when is_map(attrs) do
    %Rate{}
    |> Rate.changeset(%{rate_plan: rate_plan, amount: amount})
    |> Repo.insert()
  end
end
