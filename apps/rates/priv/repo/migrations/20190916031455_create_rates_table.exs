defmodule Rates.Repo.Migrations.CreateRatesTable do
  use Ecto.Migration

  def change do
    create table(:rates) do
      add :rate_plan, :string
      add :amount, :decimal
    end
  end
end
