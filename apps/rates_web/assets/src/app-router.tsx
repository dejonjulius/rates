import { Router } from "@reach/router"
import * as React from "react"

import { HomeRoot } from "./home"
import { SubscriptionDemoRoot } from "./subscription-demo"

import ApolloClient from "apollo-boost"

const client = new ApolloClient({
  uri: "http://localhost:4000/graphql",
})

import { ApolloProvider } from "@apollo/react-hooks"

export const AppRouter: React.SFC = (): JSX.Element => (
  <ApolloProvider client={client}>
    <Router primary={false}>
      <HomeRoot path="/" />
      <SubscriptionDemoRoot path="/subscription-demo"></SubscriptionDemoRoot>
    </Router>
  </ApolloProvider>
)
