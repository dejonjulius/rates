import { RouteComponentProps } from "@reach/router"
import { Container, Header, Table } from "semantic-ui-react"

import React from "react"

import { RATES } from "../graphql"

import { useQuery } from "@apollo/react-hooks"

export const Home: React.SFC<RouteComponentProps> = () => {
  const { loading, error, data } = useQuery(RATES)
  console.log(data, "hello")
  console.log(error)
  if (loading) return <p>Loading....</p>
  if (error) return <p>Error :(</p>

  return (
    <Container>
      <Header as="h1">Rates GenSever Demo</Header>
      <p>
        React live view with the GenServer handling the `changes of the state`{" "}
      </p>
      <Table celled>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Rate Plan</Table.HeaderCell>
            <Table.HeaderCell>Amount</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {data.rates.map(
            (rate: { ratePlan: string; amount: number }, index: number) => (
              <Table.Row key={index}>
                <Table.Cell>{rate.ratePlan}</Table.Cell>
                <Table.Cell>{rate.amount}</Table.Cell>
              </Table.Row>
            ),
          )}
        </Table.Body>
      </Table>
    </Container>
  )
}
