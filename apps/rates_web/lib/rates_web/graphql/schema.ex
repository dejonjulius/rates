defmodule RatesWeb.GraphQL.Schema do
  @moduledoc """
  The root schema
  """

  use Absinthe.Schema
  import_types(Absinthe.Type.Custom)

  object :rates do
    field(:rate_plan, :string)
    field(:amount, :float)
  end

  query do
    field :rates, list_of(:rates) do
      resolve(fn _, _, _ ->
        {:ok, Rates.list_rates()}
      end)
    end
  end

  mutation do
    field :create_rate, :rates do
      arg(:rate_plan, :string)
      arg(:amount, :float)

      resolve(fn _, attrs, _ ->
        Rates.create_rate(attrs)
      end)
    end
  end

  # subscription do
  #   field :user_added, :user do
  #     config(fn _args, _info ->
  #       {:ok, topic: "*"}
  #     end)

  #     trigger(:add_user, topic: fn _ -> ["*"] end)
  #   end
  # end
end
