defmodule RatesWeb.Router do
  use RatesWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", RatesWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/subscription-demo", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", RatesWeb do
  #   pipe_through :api
  # end

  scope "/" do
    forward("/graphql", Absinthe.Plug, schema: RatesWeb.GraphQL.Schema, json_codec: Jason, socket: RatesWeb.UserSocket)

    forward("/graphiql", Absinthe.Plug.GraphiQL,
      schema: RatesWeb.GraphQL.Schema,
      json_codec: Jason,
      socket: RatesWeb.UserSocket
    )
  end
end
