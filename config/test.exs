use Mix.Config

# Configure your database
config :rates, Rates.Repo,
  username: "postgres",
  password: "postgres",
  database: "rates_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :rates_web, RatesWeb.Endpoint,
  http: [port: 4002],
  server: false
